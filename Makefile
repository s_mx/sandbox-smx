# Makefile for sanbox-smx

TARGET=sandbox-smx
CC=g++
CFLAGS=-std=c++11 -D_DEBUG -Wall -Wextra -O2

all: $(TARGET)

$(TARGET): parser_ini.o Manager.o Parameters.o Start.o
	$(CC) core/sandbox-smx.cpp Manager.o parser_ini.o Data.o Section.o Parameters.o Start.o $(CFLAGS) -lcgroup -o $(TARGET)
	rm -rf *.o

parser_ini.o: Data.o Section.o
	$(CC) $(CFLAGS) -c core/parser_ini/parser_ini.cpp

Data.o: Section.o
	$(CC) $(CFLAGS) -c core/parser_ini/Data.cpp

Section.o:
	$(CC) $(CFLAGS) -c core/parser_ini/Section.cpp

Manager.o: Parameters.o Section.o Data.o
	$(CC) $(CFLAGS) core/resourse_manager/Manager.cpp -lcgroup -c 

Parameters.o: Section.o Data.o
	$(CC) core/resourse_manager/Parameters.cpp $(CFLAGS) -c

Start.o:
	$(CC) core/starter/Start.cpp $(CFLAGS) -c

clean:
	rm -rf *.o
	rm sandbox-smx