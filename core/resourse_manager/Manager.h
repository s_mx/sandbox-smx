#ifndef __MANAGER__H___
#define __MANAGER__H___

#include <libcgroup.h>
#include <libcgroup/groups.h>
#include "Parameters.h"
#include <exception>
#include <memory>
#include <string>

class Manager;

int sandbox_init_manager(const Parameters&, std::shared_ptr<Manager>&);

class non_initialized : std::exception {};

class Manager {
    typedef struct cgroup_controller* controller;

    struct cgroup* sandbox_group;
    controller memory_controller,
               cpu_controller;

    public:
    
    Manager (const Parameters& parameters);
    ~Manager();

    friend int start(const std::string&, std::shared_ptr<Manager>, const Parameters&);
};

#endif

