#ifndef __PARAMETERS__H__
#define __PARAMETERS__H__

#include "../parser_ini/Data.h"
#include <string>
#include <iostream>
#include <vector>

enum {ENVALL, ENVNONE, ENVADD, ENVCUSTOM};

long long cnv_memory_limit_to_ll(const std::string&);
bool check_memory_parameter(const std::string&);
bool check_cpu_share(const std::string&);
bool check_time_parameter(const std::string&);

struct Parameters {
    std::string name_prog;
    
    long long cpu_cfs_period_us = -1,       //
              cpu_cfs_quota_us = -1;        //  Стандартные поля для cgroups
    std::string cpu_share;

    // Не уверен, что в cgroups есть таймер, поэтому напишу свой как-нибудь.
    // time_limit в микросекундах
    long long time_limit = -1;

    long long memory_limit_in_bytes = -1;
    long long memory_swp_limit_in_bytes = -1;

    bool is_enviroment_occur = false;
    int enviroment_mode = 0;
    std::vector<std::pair<std::string, std::string>> enviroment_variables;

    Parameters();
    Parameters(const Data& data); 

#ifdef _DEBUG
    void print_debug() const;
#endif
};

#endif
