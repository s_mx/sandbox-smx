#include "Parameters.h"
#include "../parser_ini/Data.h"
#include <algorithm>
#include <string>
#include <vector>
#include <set>
#include <map>
#include <stdexcept>

class non_initialized : std::exception {};

std::vector<std::string> split(const std::string& str, char sep) {
    std::vector<std::string> res;
    std::string ex = "";
    for (auto elem : str) {
        if (elem == sep) {
            res.push_back(ex);
            ex = "";
        } else {
            ex += elem;
        }
    }

    res.push_back(ex); // Здесь не нужен if!!!

    return res;
}

bool check_cpu_share(const std::string& parameter) {
    std::vector<std::string> ranges = split(parameter, ',');
    for (const auto& elem : ranges) {
        std::vector<std::string> pieces;
        size_t pos;
        if ((pos = elem.find('-')) == std::string::npos) {
            pieces.push_back(elem);
        } else {
            std::string first_range = elem.substr(0, pos);
            if (elem.find('-', pos) != std::string::npos) {
                return false;
            }

            std::string second_range = elem.substr(pos + 1, elem.size() - pos - 1);
            pieces.push_back(first_range);
            pieces.push_back(second_range);
        }

        for (size_t i = 0; i < pieces.size(); i++) {
            if (pieces[i].size() == 0 || pieces[i].size() > 3) {
                return false;
            }

            if (i > 0 && stoi(pieces[0]) >= stoi(pieces[1])) {
                return false;
            }
        }
    }

    return true;
}

bool check_time_parameter(const std::string& parameter) {
    return parameter == "-1" || all_of(parameter.begin(), parameter.end(), isdigit);
}

long long cnv_memory_parameter_to_ll(const std::string& parameter) {
    if (parameter == "-1")
        return -1;
    if (all_of(parameter.begin(), parameter.end(), isdigit))
        return stoll(parameter);
    
    static std::map<char, long long> cnv_suff = { {'k', 1<<10} , {'K', 1<<10}, 
                                                  {'m', 1<<20}, {'M', 1<<20}, 
                                                  {'g', 1<<30}, {'G', 1<<30} };
    return stoll(parameter.substr(0, parameter.size() - 1)) * cnv_suff[parameter.back()];
}

bool check_memory_parameter(const std::string& parameter) {
    if (parameter.size() == 0)
        return false;
    if (parameter == "-1")
        return true;
    if (all_of(parameter.begin(), parameter.end(), isdigit))
        return true;
    
    static std::set<char> allowed_suff = {'k', 'K', 'm', 'M', 'g', 'G'};
    if (allowed_suff.find(parameter.back()) == allowed_suff.end()) {
        return false;
    }

    return parameter.size() >= 2 && all_of(parameter.begin(), prev(parameter.end()), isdigit);
}

Parameters::Parameters() {}

Parameters::Parameters(const Data& data) {
    if (!data.has_section("Main")) {
        std::cerr << "There aren't section Main in config\n";
        throw non_initialized();
    } else {
        const auto& sec = data["Main"];
        if (!sec.has_record("path")) {
            std::cerr << "There aren't path to binary file\n";
            throw non_initialized();
        }

        name_prog = sec["path"];
    }
    
    if (data.has_section("Time")) {
        const auto& section = data["Time"];

        if (section.has_record("period")) {
            if (!check_time_parameter(section["period"])) {
                std::cerr << "period isn't correct\n";
                throw non_initialized();
            }

            cpu_cfs_period_us = stoll(section["period"]);
        }

        if (section.has_record("quota")) {
            if (!check_time_parameter(section["quota"])) {
                std::cerr << "quota isn't correct\n";
                throw non_initialized();
            }

            cpu_cfs_quota_us = stoll(section["quota"]);
        }

        if (section.has_record("share")) {
            if (!check_cpu_share(section["share"])) {
                std::cerr << "cpu_share isn't correct\n";
                throw non_initialized();
            }

            cpu_share = section["share"];
        }
    }

    if (data.has_section("Memory")) {
        const auto& section = data["Memory"];
        std::string value;
        if (section.has_record("memory_limit")) {
            value = section["memory_limit"];

            if (!check_memory_parameter(value)) {
                std::cerr << "memory_limit isn't correct\n";
                throw non_initialized();
            }

            if (all_of(value.begin(), value.end(), isdigit)) {
                memory_limit_in_bytes = cnv_memory_parameter_to_ll(value);
            }
        }

        if (section.has_record("swap_memory_limit")) {
            value = section["swap_memory_limit"];

            if (!check_memory_parameter(value)) {
                std::cerr << "swap_memory_limit isn't correct\n";
                throw non_initialized();
            }

            if (all_of(value.begin(), value.end(), isdigit)) {
                memory_swp_limit_in_bytes = cnv_memory_parameter_to_ll(value);
            }
        }
    }

    std::map<std::string, int> id_mode = { {"all", ENVALL}, {"none", ENVNONE}, 
                                           {"add", ENVADD}, {"custom", ENVCUSTOM} };

    if (data.has_section("Enviroment")) {
        is_enviroment_occur = true;
        const auto& section = data["Enviroment"];
        if (section.has_record("mode")) {
            enviroment_mode = id_mode[section["mode"]];
        }

        if ((enviroment_mode == 2 && data.has_section("Enviroment_Add")) ||
            (enviroment_mode == 3 && data.has_section("Enviroment_Custom"))) {
             const Section& section2 = (enviroment_mode == 2 ? data["Enviroment_Add"] : data["Enviroment_Custom"]);
             for (auto it = section2.cbegin(); it != section2.cend(); it++) {
                 enviroment_variables.push_back({it->first, it->second});
             }
        }
    }
}

#ifdef _DEBUG

void Parameters::print_debug() const {
    std::cerr << "cpu_cfs_period_us " << cpu_cfs_period_us << "\n"
        << "cpu_cfs_quota_us " << cpu_cfs_quota_us << "\n"
        << "cpu_share" << cpu_share << "\n"
        << "time_limit " << time_limit << "\n"
        << "memory_limit_in_bytes" << memory_limit_in_bytes << "\n"; 
}

#endif

