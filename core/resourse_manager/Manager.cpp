#include <libcgroup.h>
#include <libcgroup/groups.h>
#include "Manager.h"
#include "Parameters.h"
#include <memory>
#include <iostream>

int sandbox_init_manager(const Parameters& paramaters, std::shared_ptr<Manager>& ptr_manager) {
    int ret = cgroup_init(); // не знаю, что она там вообще возвращает
    if (ret) {
        std::cerr << cgroup_strerror(ret) << "\n";
        return ret; // Может потом сделать Logger?
    }

    try {
        ptr_manager = std::shared_ptr<Manager>(new Manager(paramaters));
    } catch (const non_initialized& err) {
        return 2;
    }

    return 0;
}

Manager::Manager(const Parameters& paramaters) {
    sandbox_group = cgroup_new_cgroup("sandbox");
    memory_controller = cgroup_add_controller(sandbox_group, "memory");     // Все таки лучше по надобности добавлять контроллеры
    cpu_controller = cgroup_add_controller(sandbox_group, "cpu");           // Впрочем и так сойдет
    if (sandbox_group == nullptr || memory_controller == nullptr || 
        cpu_controller == nullptr) {
        throw non_initialized();
    }

    int ret;
    if (paramaters.cpu_cfs_period_us != -1) {
        ret = cgroup_add_value_int64(cpu_controller, "cpu.cfs_period_us", paramaters.cpu_cfs_period_us);
        if (ret) {
            std::cerr << cgroup_strerror(ret) << "\n";
            throw non_initialized();
        }

        ret = cgroup_add_value_int64(cpu_controller, "cpu.cfs_quota_us", paramaters.cpu_cfs_quota_us);
        if (ret) {
            std::cerr << cgroup_strerror(ret) << "\n";
            throw non_initialized();
        }
    }

    if (paramaters.cpu_share != "") {
        ret = cgroup_add_value_string(cpu_controller, "cpu.shares", paramaters.cpu_share.c_str());
        if (ret) {
            std::cerr << cgroup_strerror(ret) << "\n";
            throw non_initialized();
        }
    }

    if (paramaters.memory_limit_in_bytes != -1) {
        ret = cgroup_add_value_int64(memory_controller, "memory.limit_in_bytes", paramaters.memory_limit_in_bytes);
        if (ret) {
            std::cerr << cgroup_strerror(ret) << "\n";
            throw non_initialized();
        }
    }

    if (paramaters.memory_swp_limit_in_bytes != -1) {
        ret = cgroup_add_value_int64(memory_controller, "memory.memsw.memory_limit_in_bytes", paramaters.memory_swp_limit_in_bytes);
        if (ret) {
            std::cerr << cgroup_strerror(ret) << "\n";
            throw non_initialized();
        }
    }

    ret = cgroup_create_cgroup(sandbox_group, 0);
    if (ret) {
        std::cerr << cgroup_strerror(ret) << "\n";
        throw non_initialized();
    }
}

Manager::~Manager() {
    cgroup_delete_cgroup(sandbox_group, 0);
    cgroup_free_controllers(sandbox_group);
    cgroup_free(&sandbox_group);
}

