#include <libcgroup.h>
#include "parser_ini/parser_ini.h"
#include "resourse_manager/Manager.h"
#include "starter/Start.h"
#include <fcntl.h>
#include <unistd.h>
#include <iostream>
#include <memory>

void usage() {
    std::cout << "Usage: [-c name_config]\n";
    std::cout << "-c name_config - Name of configuration file.\n";
    std::cout << "If it doesn't occur the sandbox.conf will used instead.\n";
}

bool find_config(const std::string&& name_conf = "sandbox.conf") {
    int fd = open(name_conf.c_str(), O_RDONLY, 0);
    if (fd != -1) {
        close(fd);
        return true;
    }

    return false;
}

int main(int argc, char *argv[]) {
    int res_opt;
    Parameters parameters;
    std::string name_config;
    while ((res_opt = getopt(argc, argv, "c:")) != -1) {
        if (find_config(optarg)) {
            name_config = std::string(optarg);
            parameters = Parameters(parse_ini(name_config));
        } else {
            usage();
            std::cerr << "There isn't config file\n";
            return 0;
        }
    }  

    if (name_config == "") {
        if (find_config()) {
            parameters = parse_ini("sandox_default.conf");           
        } else {
            usage();
            std::cerr << "There isn't config file\n";
            return 0;
        }
    }

    std::shared_ptr<Manager> ptr;

    int ret = sandbox_init_manager(parameters, ptr);
    if (ret) {
        std::cerr << "Init manager failed\nIt is possible you have weak rights\n";
        return 0;
    }

    ret = start(parameters.name_prog, ptr, parameters);
    if (ret) {
        std::cerr << "something goes wrong, code: " << ret << "\n";
    } else {
        std::cerr << "end process\n";
    }
}
