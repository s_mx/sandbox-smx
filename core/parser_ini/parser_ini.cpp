#include "parser_ini.h"
#include "Data.h"
#include "Section.h"
#include <string>
#include <fstream>
#include <utility>
#include <iostream>
using std::string;

// Simple version

std::pair<std::string, std::string> split_record(const std::string& line) {
    std::pair<string, string> record = {"", ""};
    bool flag = false;
    for (size_t pos = 0; pos < line.size(); pos++) {
        if (line[pos] == '=' && !flag) {
            flag = true;
            continue;
        }

        (!flag ? record.first : record.second) += line[pos];
    }

    return record;
}

std::string strip(const std::string& str) {
    std::string res = "",
                ex = "";

    size_t i = 0;
    for (; i < str.size(); i++) {
        if (!isspace(str[i]))
            break;
    }

    for (; i < str.size(); i++) {
        ex += str[i];
        if (!isspace(str[i])) {
            res += ex;
            ex = "";
        }
    }

    return res;
}

Data parse_ini(const string& name_conf) {
    Data data;
    std::ifstream input_conf(name_conf);

    Section cur_section;
    string name_cur_section, line;
    bool was_read_first_section = false;
    while (getline(input_conf, line)) {
        line = strip(line);
        if (line.size() == 0 || line[0] == ';' || line[0] == '#')
            continue;
        if (line[0] == '[') {
            name_cur_section = "";
            for (size_t pos = 1; pos < line.size() && line[pos] != ']'; pos++) {
                name_cur_section += line[pos];
            }

            if (was_read_first_section)
                data[cur_section.name_section()] = cur_section;
            cur_section = Section(name_cur_section);
            was_read_first_section = true;
            continue;
        }

        auto record = split_record(line);
        cur_section[record.first] = record.second;
    }

    if (cur_section.size()) {
        data[cur_section.name_section()] = cur_section;
    }

    return data; 
}
