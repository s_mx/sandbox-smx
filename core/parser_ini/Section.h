#ifndef __SECTION__H__
#define __SECTION__H__

#include <string>
#include <vector>
#include <stddef.h>
#include <map>

using std::string;

class Section {
private:

    string name_section_;
    std::map<string, string> records_;

public:

    Section() = default;
    Section(const string& name);

    size_t size() const;
    const string& name_section() const;
    bool has_record(const std::string&) const;
    const string& operator[] (const string&) const;
    string& operator[] (const string&);
    
    typedef std::map<string, string>::iterator Iter;
    typedef std::map<string, string>::const_iterator CIter;

    Iter begin();
    Iter end();
    CIter cbegin() const;
    CIter cend() const;

#ifdef _DEBUG
    void print_debug() const;
#endif

};

#endif
