#include "Data.h"
#include <stddef.h>
#include <vector>
#include <string>
#include <iostream>

size_t Data::size() const {
    return sections_.size();
}

bool Data::has_section(const std::string& name_section) const {
    return sections_.count(name_section);
}

const Section& Data::operator[] (const string& key) const {
    return sections_.at(key);
}

Section& Data::operator[] (const string& key) {
    return sections_[key];
}

typedef std::map<string, Section>::iterator Iter;

Iter Data::begin() {
    return sections_.begin();
}

Iter Data::end() {
    return sections_.end();
}

typedef std::map<string, Section>::const_iterator CIter;

CIter Data::cbegin() const {
    return sections_.cbegin();
}

CIter Data::cend() const {
    return sections_.cend();
}

#ifdef _DEBUG

void Data::print_debug() const {
    std::cerr << size() << "\n";
    for (const auto& sec : sections_) {
        std::cerr << "name section: " << sec.first << "\n";
        for (auto rec = sec.second.cbegin(); rec != sec.second.cend(); rec++) {
            std::cerr << "   " << rec->first << " " << rec->second << "\n";
        }

        std::cerr << "\n";
    }
}

#endif
