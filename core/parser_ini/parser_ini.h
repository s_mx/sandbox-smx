#ifndef __PARSER__INI__H__
#define __PARSER__INI__H__

#include "Data.h"
#include <string>
#include <utility>

std::pair<std::string, std::string> split_record(const std::string&);
Data parse_ini(const std::string&);

#endif
