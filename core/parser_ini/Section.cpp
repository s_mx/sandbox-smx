#include "Section.h"
#include <iostream>
#include <vector>
#include <string>
#include <stddef.h>

using std::string;

Section::Section(const string& name) : name_section_(name) {
}

size_t Section::size() const {
    return records_.size();
}

const string& Section::name_section() const {
    return name_section_;
}

bool Section::has_record(const std::string& name_record) const {
    return records_.count(name_record);
}

const string& Section::operator[] (const string& key) const {
    return records_.at(key);
}

string& Section::operator[] (const string& key) {
    return records_[key];
}

typedef std::map<string, string>::iterator Iter;

Iter Section::begin() {
    return records_.begin();
}

Iter Section::end() {
    return records_.end();
}

typedef std::map<string, string>::const_iterator CIter;

CIter Section::cbegin() const {
    return records_.cbegin();
}

CIter Section::cend() const {
    return records_.cend();
}

#ifdef _DEBUG

void Section::print_debug() const {
    for (auto rec = cbegin(); rec != cend(); rec++) {
        std::cerr << rec->first << " " << rec->second << "\n";
    }
}

#endif

