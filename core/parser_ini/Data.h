#ifndef __DATA__H__
#define __DATA__H__

#include "Section.h"
#include <vector>
#include <string>
using std::string;

class Data {
private:
    
    std::map<string, Section> sections_;

public:

    size_t size() const;
    bool has_section(const std::string&) const;
    const Section& operator[] (const string&) const;
    Section& operator[] (const string&);

    typedef std::map<string, Section>::iterator Iter;
    typedef std::map<string, Section>::const_iterator CIter;

    Iter begin();
    Iter end();
    CIter cbegin() const;
    CIter cend() const;

#ifdef _DEBUG
    void print_debug() const;
#endif
};

#endif
