#ifndef __START_H__
#define __START_H__

#include "../resourse_manager/Manager.h"
#include "../resourse_manager/Parameters.h"
#include <string>
#include <memory>

void setup_environ(const Parameters&);
int start(const std::string& name_prog, std::shared_ptr<Manager>, const Parameters&);

#endif
