#include <libcgroup.h>
#include <libcgroup/tasks.h>
#include "Start.h"
#include "../resourse_manager/Manager.h"
#include "../parser_ini/parser_ini.h"
#include <unistd.h>
#include <wait.h>
#include <algorithm>
#include <string>
#include <iostream>
#include <memory>

void setup_environ(const Parameters& parameters) {
    if (parameters.is_enviroment_occur) {
        if (parameters.enviroment_mode == ENVNONE) {
            delete [] environ;
            environ = new char* [1];
            environ[0] = NULL;
        } else if (parameters.enviroment_mode == ENVADD) {
            std::map<std::string, std::string> records;
            auto ptrenv = environ;
            while (*ptrenv != NULL) {
                std::pair<std::string, std::string> record = split_record(std::string(*ptrenv));
                records[record.first] = record.second;
                ++ptrenv;
            }

            const auto& vars = parameters.enviroment_variables;
            for (const auto& record : vars) {
                records[record.first] = record.second;
            }

            delete [] environ;
            environ = new char* [records.size() + 1];
            int ind = 0;
            for (const auto& record : records) {
                std::string str_record = record.first;
                str_record += "=";
                str_record += record.second;
                environ[ind] = new char[str_record.size() + 1];
                std::copy(str_record.c_str(), str_record.c_str() + str_record.size(), environ[ind++]);
            }

            environ[records.size()] = NULL;
        } else if (parameters.enviroment_mode == ENVCUSTOM) {
            delete [] environ;
            const auto& vars = parameters.enviroment_variables;
            environ = new char* [vars.size() + 1];
            for (size_t i = 0; i < vars.size(); i++) {
                std::string all_record = vars[i].first;
                all_record += "=";
                all_record += vars[i].second;
                environ[i] = new char [all_record.size() + 1];
                std::copy(all_record.c_str(), all_record.c_str() + all_record.size(), environ[i]);
            }

            environ[vars.size()] = NULL;
        }
    }
}

int start(const std::string& path, std::shared_ptr<Manager> ptr, const Parameters& parameters) {
    std::cerr << "start process\n";
    pid_t pid = fork();
    int ret;
    if (!pid) {
        ret = cgroup_attach_task(ptr->sandbox_group);
        if (ret) {
            std::cerr << cgroup_strerror(ret) << "\n";
            exit(0);
        }

        ret = cgroup_modify_cgroup(ptr->sandbox_group);
        if (ret) {
            std::cerr << cgroup_strerror(ret) << "\n";
            exit(0);
        }

        setup_environ(parameters);
        execlp(path.c_str(), path.c_str(), NULL);
    }

    waitpid(pid, NULL, 0);
    // Тут наверное должны быть всякие таймеры    
    return 0;
}
